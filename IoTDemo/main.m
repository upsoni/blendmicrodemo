//
//  main.m
//  IoTDemo
//
//  Created by John Keogh on 1/12/15.
//  Copyright (c) 2015 EyesBot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

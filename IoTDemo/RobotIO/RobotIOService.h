//
//  RobotIOService.h
//  EyesBot Driver
//
//  Created by John Keogh on 9/2/14.
//
//

#import <Foundation/Foundation.h>

#import "BLE.h"


#define kBLEProximityChanged @"ProximityChanged"

@interface RobotIOService : NSObject<BLEDelegate>{
    BLE *bleShield;
    NSTimer *rssiTimer;
    NSTimer *connectionTimer;
    NSMutableString *incomingBuffer;
}


+(id)sharedInstance;

@property BOOL connected;
@property NSInteger proximity;

- (void)scanForBLEDevices;
- (void)selectBLEDevice:(NSInteger)row;
- (void)disconnectFromBLEDevices;

-(void)sendTilt:(int)tilt;
-(void)readProximity;



-(NSArray *)bleDevices;


@end

/*
 
 Code for turning on a motor and reading a sensor via Bluetooth using the
 RedBear Labs library.
 
 This source is free and unencumbered software released into the public
 domain.  Source referenced by this source may have other copyrights, it
 is your responsibility to abide by all copyrights.
 
 Anyone is free to copy, modify, publish, use, compile, sell, or
 distribute this software, either in source code form or as a compiled
 binary, for any purpose, commercial or non-commercial, and by any
 means.
 
 In jurisdictions that recognize copyright laws, the author or authors
 of this software dedicate any and all copyright interest in the
 software to the public domain. We make this dedication for the benefit
 of the public at large and to the detriment of our heirs and
 successors. We intend this dedication to be an overt act of
 relinquishment in perpetuity of all present and future rights to this
 software under copyright law.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 
 Written by John Keogh of EyesBot, LLC (john@eyesbot.com)
 
 */


#import "RobotIOService.h"


@implementation RobotIOService

@synthesize connected, proximity;

+ (id)sharedInstance
{
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init]; // or some other init method
    });
    return _sharedObject;
}

- (id) init
{
    if (self = [super init])
    {
        bleShield = [[BLE alloc] init];
        [bleShield controlSetup];
        bleShield.delegate = self;
        incomingBuffer = [[NSMutableString alloc] init];
    }
    return self;
}


#pragma mark - state to or from robot

-(void)sendTilt:(int)tilt{
    if (bleShield.activePeripheral){
        if(bleShield.activePeripheral.state == CBPeripheralStateConnected)
        {
            NSString *powerString = [NSString stringWithFormat:@"TILT:%i;", tilt];
            [bleShield write:[powerString dataUsingEncoding:NSUTF8StringEncoding]];
        }
    }
}

-(void)readProximity{
    if (bleShield.activePeripheral){
        if(bleShield.activePeripheral.state == CBPeripheralStateConnected)
        {
            NSString *proximityString = [NSString stringWithFormat:@"PROX:;"];
            [bleShield write:[proximityString dataUsingEncoding:NSUTF8StringEncoding]];
        }
    }
}

#pragma mark - BLE code


-(void) readRSSITimer:(NSTimer *)timer
{
    [bleShield readRSSI];
}


-(void) connectionTimer:(NSTimer *)timer
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kBLEPeripheralsListChanged object:nil];
    if(bleShield.peripherals.count > 0)
    {
        //[bleShield connectPeripheral:[bleShield.peripherals objectAtIndex:0]];
    }
    else
    {
        //[activityIndicator stopAnimating];
        //self.navigationItem.leftBarButtonItem.enabled = YES;
    }
}

- (void)scanForBLEDevices
{
    
    if(bleShield.CM.state != CBCentralManagerStatePoweredOn){
        return;
    }
    
    //fix this
    if (bleShield.activePeripheral){
        if(bleShield.activePeripheral.state == CBPeripheralStateConnected)
        {
            [[bleShield CM] cancelPeripheralConnection:[bleShield activePeripheral]];
            return;
        }
    }
    
    if (bleShield.peripherals)
        bleShield.peripherals = nil;

    [bleShield.peripherals removeAllObjects];
    
    [bleShield findBLEPeripherals:3.0];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [NSTimer scheduledTimerWithTimeInterval:(float)3.0 target:[RobotIOService sharedInstance] selector:@selector(connectionTimer:) userInfo:nil repeats:NO];
    });
    
    //[activityIndicator startAnimating];
    //self.navigationItem.leftBarButtonItem.enabled = NO;
}

- (void)disconnectFromBLEDevices
{
    if (bleShield.activePeripheral){
        if(bleShield.activePeripheral.state == CBPeripheralStateConnected)
        {
            [[bleShield CM] cancelPeripheralConnection:[bleShield activePeripheral]];
            return;
        }
    }
    
    if (bleShield.peripherals)
        bleShield.peripherals = nil;
    
}


-(void) bleDidReceiveData:(unsigned char *)data length:(int)length
{
    NSData *d = [NSData dataWithBytes:data length:length];
    NSString *s = [[NSString alloc] initWithData:d encoding:NSUTF8StringEncoding];
    [incomingBuffer appendString:s];
    
    NSRange delimiterIndex =[incomingBuffer rangeOfString:@";"];
    if( delimiterIndex.location!= NSNotFound){
        NSString *incoming = [incomingBuffer substringToIndex:delimiterIndex.location];
        int incomingLength=[incoming length];
        char power [4];
        const char *command = [incoming UTF8String];
        for(int offset=0; (offset<3)&&(command[offset+5]!='\0'); offset++){
            power[offset]=command[offset+5];
            power[offset+1]='\0';
        }
        self.proximity = atoi(power);
        [[NSNotificationCenter defaultCenter] postNotificationName:kBLEProximityChanged object:nil];
        [incomingBuffer deleteCharactersInRange:NSMakeRange(0, incomingLength+1)];
    }
    
    //NSLog(@"%@", s);
}

- (void) bleDidDisconnect
{
    NSLog(@"bleDidDisconnect");
    
    self.connected=NO;
}

-(void) bleDidConnect
{
    self.connected=YES;
    NSLog(@"bleDidConnect");
}


-(NSArray *)bleDevices{
    NSMutableArray *bleArray = [[NSMutableArray alloc]init];
    
    for(CBPeripheral * cbPeripheral in bleShield.peripherals){
        [bleArray addObject:cbPeripheral.name];
    }
    
    return [bleArray copy];
}


- (void)selectBLEDevice:(NSInteger)row{
    if(row<bleShield.peripherals.count){
        [bleShield connectPeripheral:[bleShield.peripherals objectAtIndex:row]];
    }
}

@end

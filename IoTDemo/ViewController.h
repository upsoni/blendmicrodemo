//
//  ViewController.h
//  IoTDemo
//
//  Created by John Keogh on 1/12/15.
//  Copyright (c) 2015 EyesBot. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CMMotionManager;

@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    NSTimer *scannerTimer;
    NSTimer *proximityTimer;
    BOOL buttonPressed;
    int lastTilt;
}

@property (weak, nonatomic) IBOutlet UILabel *deviceConnectedLabel;
@property (weak, nonatomic) IBOutlet UITableView *deviceNamesTable;
@property (weak, nonatomic) IBOutlet UIButton *disconnectDeviceButton;
@property (weak, nonatomic) IBOutlet UIButton *controlDeviceButton;
@property (weak, nonatomic) IBOutlet UIButton *proximityButton;//using a button in case I want to turn on/off

@property (strong, nonatomic) NSArray *bleDeviceNames;
@property (strong, nonatomic) CMMotionManager *motionManager;
@property (strong, nonatomic) NSOperationQueue *operationQueue;


@end

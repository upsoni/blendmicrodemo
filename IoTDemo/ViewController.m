/*
 
 Code for turning on a motor and reading a sensor via Bluetooth using the
 RedBear Labs library.
 
 This source is free and unencumbered software released into the public
 domain.  Source referenced by this source may have other copyrights, it
 is your responsibility to abide by all copyrights.
 
 Anyone is free to copy, modify, publish, use, compile, sell, or
 distribute this software, either in source code form or as a compiled
 binary, for any purpose, commercial or non-commercial, and by any
 means.
 
 In jurisdictions that recognize copyright laws, the author or authors
 of this software dedicate any and all copyright interest in the
 software to the public domain. We make this dedication for the benefit
 of the public at large and to the detriment of our heirs and
 successors. We intend this dedication to be an overt act of
 relinquishment in perpetuity of all present and future rights to this
 software under copyright law.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 
 Written by John Keogh of EyesBot, LLC (john@eyesbot.com)
 
 */

#import <CoreMotion/CoreMotion.h>


#import "ViewController.h"

#import "RobotIO/RobotIOService.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize deviceConnectedLabel, deviceNamesTable, disconnectDeviceButton, controlDeviceButton, proximityButton;

@synthesize bleDeviceNames;

- (void)viewDidLoad {
    [super viewDidLoad];
    buttonPressed = NO;
    [controlDeviceButton setBackgroundColor:[UIColor lightGrayColor]];
    
    [self.disconnectDeviceButton setBackgroundColor:[UIColor redColor]];
    [self.disconnectDeviceButton.layer setBorderWidth:2.0f];
    self.disconnectDeviceButton.layer.cornerRadius = 8;
    [self.disconnectDeviceButton.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    self.proximityButton.hidden = YES;

    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(peripheralCountChanged:)
                                                 name:kBLEPeripheralsListChanged  object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(proximityChanged:)
                                                 name:kBLEProximityChanged  object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated{
    
    self.motionManager = [[CMMotionManager alloc] init];
    self.motionManager.accelerometerUpdateInterval = 0.5;
    self.operationQueue = [[NSOperationQueue alloc] init];
    
    if ( ![self.motionManager isAccelerometerAvailable] ) {
        return;
    }
    
    __weak ViewController *weakSelf = self;
    [self.motionManager startAccelerometerUpdatesToQueue:self.operationQueue withHandler:^(CMAccelerometerData *accelerometerData, NSError *error) {
        [weakSelf accelerometerUpdateWithData:accelerometerData error:error];
    }];

    self.deviceNamesTable.hidden = false;
    self.disconnectDeviceButton.hidden = true;
    self.deviceConnectedLabel.hidden = true;
    
    scannerTimer = [NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(bleScannerTimer:) userInfo:nil repeats:YES];
    
    [super viewWillAppear:animated];
}

- (void) startLookingForDevices
{
    
    [[RobotIOService sharedInstance] scanForBLEDevices];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    [scannerTimer invalidate];
    scannerTimer = nil;
    
    [[RobotIOService sharedInstance] disconnectFromBLEDevices];
}


#pragma mark - BLE Code


-(void)bleScannerTimer:(NSTimer *)timer {
    [self startLookingForDevices];
}

-(void)proximityTimer:(NSTimer *)timer {
    [[RobotIOService sharedInstance] readProximity];
}

-(void)peripheralCountChanged:(NSNotification *) notification{
    self.bleDeviceNames = [[RobotIOService sharedInstance] bleDevices];
    [self.deviceNamesTable reloadData];
}

-(void)proximityChanged:(NSNotification *) notification{
    int newProximity = (int)[[RobotIOService sharedInstance] proximity];
    UIColor *color;
    NSLog(@"prox %i", newProximity);
    if(newProximity<150){
        color = [UIColor darkGrayColor];
    }
    else if(newProximity<300){
        color = [UIColor grayColor];
    }
    else if(newProximity<600){
        color = [UIColor lightGrayColor];
    }
    else{
        color = [UIColor whiteColor];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.proximityButton.backgroundColor = color;
    });
    
    
}


#pragma mark - Table for devices

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if((self.bleDeviceNames == nil)||(self.bleDeviceNames.count == 0)){
        return 1;
    }
    return self.bleDeviceNames.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Identifier for retrieving reusable cells.
    static NSString *cellIdentifier = @"BLECellID";
    
    // Attempt to request the reusable cell.
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    // No cell available - create one.
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
    }
    
    // Set the text of the cell to the row index.
    NSString *title = @"";
    
    if((self.bleDeviceNames == nil)||(self.bleDeviceNames.count == 0)){
        title = @"Searching for devices....";
    }
    else if(indexPath.row<[self.bleDeviceNames count]){
        title = [self.bleDeviceNames objectAtIndex:indexPath.row];
    }
    cell.textLabel.text = title;
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int row = indexPath.row;
    
    if(row <self.bleDeviceNames.count){
        [scannerTimer invalidate];
        scannerTimer = nil;
        proximityTimer = [NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(proximityTimer:) userInfo:nil repeats:YES];
        self.proximityButton.hidden = NO;
        [[RobotIOService sharedInstance] selectBLEDevice:row];
        self.deviceNamesTable.hidden = true;
        self.disconnectDeviceButton.hidden = false;
        self.deviceConnectedLabel.hidden = false;
        self.deviceConnectedLabel.text = [NSString stringWithFormat:@"Now connected to %@", [self.bleDeviceNames objectAtIndex:row]];
    }
}

#pragma mark - Button Events


- (IBAction)disconnectDevicePushed:(id)sender{
    scannerTimer = [NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(bleScannerTimer:) userInfo:nil repeats:YES];
    [[RobotIOService sharedInstance] disconnectFromBLEDevices];
    [proximityTimer invalidate];
    self.proximityButton.hidden = YES;
    proximityTimer = nil;
    self.deviceNamesTable.hidden = false;
    self.disconnectDeviceButton.hidden = true;
    self.deviceConnectedLabel.hidden = true;
}

- (IBAction)controlMotorPushed:(id)sender{
    buttonPressed = !buttonPressed;
    
    if(buttonPressed){
        [controlDeviceButton setBackgroundColor:[UIColor whiteColor]];
    }
    else{
        [controlDeviceButton setBackgroundColor:[UIColor lightGrayColor]];
    }
}


#pragma mark - iOS device accelerometer code

- (void)accelerometerUpdateWithData:(CMAccelerometerData *)accelerometerData error:(NSError *)error
{
    if ( error ) {
        return;
    }
    
    if(buttonPressed){
    
        CMAcceleration acceleration = accelerometerData.acceleration;
        
        double tilt = (acceleration.y+1.0)/2;
        if(tilt<0)
            tilt=0;
        if(tilt>1.0)
            tilt=1.0;
        
        tilt*=255;//should do this in the arduino, but I'm lazy
        
        if(tilt!=lastTilt){
            lastTilt = tilt;
            [[RobotIOService sharedInstance] sendTilt:(int)tilt];
        }
    }
}



@end

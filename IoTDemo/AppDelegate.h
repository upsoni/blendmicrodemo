//
//  AppDelegate.h
//  IoTDemo
//
//  Created by John Keogh on 1/12/15.
//  Copyright (c) 2015 EyesBot. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

